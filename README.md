##For start 
`git clone https://NarekSahakyan@bitbucket.org/NarekSahakyan/avax-test-2-vue.git`

`cd avax-test-2-vue`

Need to have global installed node.js and npm

`npm install`

`npm run serve`

## Possibilities and Features

There are 2 industries / specializations and one candidate / employee by default.

User can add new candidate

User can mention if some document or material is required or not

User can add/delete to/from list options for each industry

User can see each candidates detailed information about OnBoarding status (click on "Pencil" icon on candidates page)

How well is filled information for each candidate can be seen with "Stars" on candidates table

If section for candidate is green filled, it's mean that he collect all required data
