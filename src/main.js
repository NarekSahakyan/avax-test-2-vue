import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VeeValidate from 'vee-validate';
import VueGlobalVariable from 'vue-global-var';

// Components
import App from './App.vue'
import AddCandidate from './components/AddCandidate.vue'
import ContractCard from './components/ContractCard.vue'
import EmployeeDetails from './components/EmployeeDetails.vue'
import NotFound from './components/404.vue'

Vue.config.productionTip = false;

Vue.use(Router);
Vue.use(BootstrapVue);
Vue.use(VeeValidate);

// Global variables
import {getEmployees} from './configJSON'
import {getIndustries} from './configJSON'
Vue.use(VueGlobalVariable, {
	globals: {
		employees: getEmployees(),
		industries: getIndustries()
	},
});

const router = new Router({
	mode: 'history',
	routes: [
		{path: '*', component: NotFound, name: '404'},
		{path: '/', component: AddCandidate, name: 'addCandidate'},
		{path: '/contractCard', component: ContractCard, name: 'contractCard'},
		{path: '/employeeDetails/:id', component: EmployeeDetails, name: 'employeeDetails'},
	]
});

new Vue({
	router,
	mode: 'history',
	render: h => h(App)
}).$mount('#app');
