export const getIndustries = function () {
	return require("./data/industries.json");
};
export const getEmployees = function () {
	return require("./data/employees.json");
};
